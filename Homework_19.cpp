﻿#include <iostream>

class Animal 
{
public:
    virtual void Voice()
    {}
};

class Dog: public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woff!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo!\n";
    }
};


int main()
{
    Animal* animals[5] = {
        new Dog(),
        new Cat(),
        new Cow(),
        new Cat(),
        new Dog()
    };

    for (Animal* animal : animals)
    {
        animal->Voice();
    }

    for (Animal* animal : animals)
    {
        delete animal;
    }
}
